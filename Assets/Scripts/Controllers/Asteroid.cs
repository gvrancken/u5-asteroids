﻿using UnityEngine;
using System.Collections;

public class Asteroid : MonoBehaviour {

    [System.Serializable]
    public class SpawnWhenDead {
        public GameObject spawnGO;
        public int amount = 2;
    }

    public SpawnWhenDead[] spawnData;

    public float HitPoints = 10f;

    public float maxRandomMovement;

    float randomMovement;

	// Use this for initialization
	void Start () {
        randomMovement = Random.Range(-maxRandomMovement, maxRandomMovement);

        GetComponentInChildren<Rigidbody>().AddForce(new Vector3(randomMovement,0,randomMovement), ForceMode.Impulse);
        GetComponentInChildren<Rigidbody>().AddTorque(Vector3.one);

	}
	
	// Update is called once per frame
	void Update () {
       
	}

    public void TakeDamage (float amount) {
        HitPoints -= amount;

        if (HitPoints <= 0)
        {
            // we die!
            if (spawnData[0] != null)
            {
                for (int i = 0; i < spawnData[0].amount; i++)
                {
                    Instantiate(spawnData[0].spawnGO, transform.position, transform.rotation);
                }
            }

            Destroy(gameObject);
        }
    }

}



