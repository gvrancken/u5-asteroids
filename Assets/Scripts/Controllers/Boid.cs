﻿using UnityEngine;
using System.Collections.Generic;

public class Boid : MonoBehaviour {

    public float movePercentage = 100f;
    public float maxSpeed = 100f;
    public float avoidDistance = 1.3f;
    public float avoidPredatorBase = 1f;
    public float rotationSpeed = 5f;
    public float flockMaxDistance = 2f;
    public float maxRuleDelay = 5f;

    Boid[] boids;
    Predator[] predators;
    Vector3 averagePosition;
    Vector3 _velocity;
    int flockSize;
    Target target;

    float currentAdrenaline = 0;
    float maxAdrenaline = 10f;
    float coolDown;

    // Use this for initialization
    void Start () {
        boids = GameObject.FindObjectsOfType<Boid>();
        predators = GameObject.FindObjectsOfType<Predator>();
        target = GameObject.FindObjectOfType<Target>();
    }

    // Update is called once per frame
    void Update () 
    {
        // add all rules to velocity
        if (Random.Range(0, maxRuleDelay - (currentAdrenaline/23f * 5f)) < 1)
        {
            _velocity = Vector3.zero;
            _velocity += 1.0f * MoveToPerceivedAverage();
            _velocity += 1.0f * AvoidCollision();
            _velocity += 0.4f * AdaptToVelocityOfGroup();
            _velocity += 1.0f * MoveToGoal();
        }

        // always be wary of predators, so if they are in range, act immediately.
//        velocity += 5.0f * AvoidPredator();

        // generate adrenaline modifier based on velocity
        float rawAdrenaline = (currentAdrenaline / maxAdrenaline) * maxSpeed;
        if (rawAdrenaline <= _velocity.magnitude)
        {
            currentAdrenaline = _velocity.magnitude / maxSpeed * maxAdrenaline;
        }
        else
        {
            currentAdrenaline -= 12f * Time.deltaTime;
            _velocity = _velocity.normalized * currentAdrenaline;
        }

        if (_velocity.magnitude > maxSpeed)
        {
            _velocity = _velocity.normalized * maxSpeed;
        }

        transform.Translate(0, 0, _velocity.magnitude * Time.deltaTime);

        float rotationMultiplier = 1f;
        if (currentAdrenaline > 10)
        {
            rotationMultiplier = 1.2f;
        }

        if (_velocity != Vector3.zero)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation,
                Quaternion.LookRotation(_velocity),
                rotationSpeed * rotationMultiplier * Time.deltaTime);
        }

        // create a new target
//        if (Vector3.SqrMagnitude(averagePosition - target.transform.position) < 25)
//        {
//            target.SetNewPosition();
//        }

    }

    Vector3 MoveToGoal() 
    {
//        Debug.DrawLine(transform.position, target.transform.position, Color.blue);
        return (target.GetPosition() - transform.position);
    }



    Vector3 MoveToPerceivedAverage() 
    {
        Vector3 r1_perceivedAverage = Vector3.zero;
        Vector3 c = Vector3.zero;
        Vector3 r3_perceivedVelocity = Vector3.zero;
        flockSize = 0;
        foreach (Boid boid in boids)
        {
            if (boid != this)
            {
                // become friends with nearby fishes
                if (Vector3.Distance(this.transform.position, boid.transform.position) < flockMaxDistance)
                {
                    flockSize++;
                    r1_perceivedAverage += boid.transform.position;
                }
            }
        }

        if (flockSize == 0)
        {
            return c;
        }

        r1_perceivedAverage /= flockSize;
//        Debug.DrawLine(transform.position, r1_perceivedAverage, Color.green);
        return ( r1_perceivedAverage - this.transform.position );
    }



    Vector3 AvoidCollision() 
    {
        Vector3 c = Vector3.zero;

        foreach (Boid boid in boids)
        {
            if (boid != this)
            {
                if (Vector3.Distance(boid.transform.position, this.transform.position) < avoidDistance)
                {
                    c = c - (boid.transform.position - this.transform.position);
                }
            }
        }

//        if (c.magnitude < 3f)
//        {
//            c = c.normalized * 3;
//        }
//        Debug.DrawLine(transform.position, transform.position+c, Color.black);
        return c;
    }



    Vector3 AdaptToVelocityOfGroup() 
    {
        Vector3 r3_perceivedVelocity = Vector3.zero;
        flockSize = 0;
        foreach (Boid boid in boids)
        {
            if (boid != this)
            {
                if (Vector3.Distance(this.transform.position, boid.transform.position) <= flockMaxDistance)
                {
                    flockSize++;
                    r3_perceivedVelocity += boid._velocity;
                }
            }
        }

        if (flockSize == 0)
        {
            return _velocity;
        }

        r3_perceivedVelocity = r3_perceivedVelocity / flockSize;

//        Debug.DrawLine(transform.position, transform.position+r3_perceivedVelocity, Color.yellow);
        return r3_perceivedVelocity;
    }



    Vector3 AvoidPredator() 
    {
        Vector3 c = Vector3.zero;

        foreach (Predator predator in predators)
        {
            float avoidDistance = (avoidPredatorBase * predator.AvoidMultiplier);
            if (Vector3.SqrMagnitude(predator.transform.position - this.transform.position) < avoidDistance * avoidDistance)
            {
                float m = avoidDistance - Vector3.Distance(predator.transform.position, this.transform.position);
                c = c - ((predator.transform.position - this.transform.position) * m );
            }
        }

//        Debug.DrawLine(transform.position, transform.position+c, Color.red);

        return c;
    }
}
