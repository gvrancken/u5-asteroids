﻿using UnityEngine;
using System.Collections.Generic;

public class BoidsManager : MonoBehaviour {

    public int numBoids = 200;

    List<Boid> Boids;
    public GameObject boidPrefab;

	// Use this for initialization
	void Start () {
        Transform boids = new GameObject("Boids").transform;
        for (int i = 0; i < numBoids; i++) {
            GameObject boid = ( GameObject ) Instantiate(boidPrefab, Random.insideUnitSphere, Random.rotation);
            boid.transform.SetParent(boids);
        }

	}

}
