﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public float damage = 1f;
    public float speed = 50f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        transform.position += transform.forward * speed * Time.deltaTime;

        if (Vector3.Distance(transform.position, Vector3.zero) > 300f)
        {
            Destroy(gameObject);
        }

	}

    void OnCollisionEnter(Collision col) {  
        if (col.transform.GetComponent<Asteroid>() != null)
        {
            col.transform.GetComponent<Asteroid>().TakeDamage(damage);
            col.transform.GetComponent<Rigidbody>().AddForceAtPosition(transform.forward * 100, col.contacts[0].point);
        }

        Destroy(gameObject); 

    }


}
