﻿using UnityEngine;
using System.Collections;

public class EnemyInputController : InputController {

    bool SeePlayer = false;
    float maxCoolDown = 2f;
    float coolDown = 0;

    delegate void CoolDownHandler();
    CoolDownHandler OnCoolDown;

    void Start () {
        
    }

    // Update is called once per frame
    void Update () {

        coolDown -= Time.deltaTime;

        if (coolDown > 0)
        {
            OnCoolDown();
        }

        IsFireBtnDown = false;

        Debug.DrawRay(transform.position, transform.forward * 100f, Color.red);

        RaycastHit hitInfo;

        if (Physics.Raycast(transform.position, transform.forward, out hitInfo, 100f, 1 << LayerMask.NameToLayer("Ship")))
        {
            // we see the player
            if (!SeePlayer)
            {
                Debug.Log("seen him!");
                if (IsSteerLeftBtnDown)
                {
                    IsSteerLeftBtnDown = false;
                    coolDown = maxCoolDown;
                    OnCoolDown = () =>
                    {
                        IsSteerRightBtnDown = true;
                    };
                }
                if (IsSteerRightBtnDown)
                {
                    IsSteerRightBtnDown = false;
                    coolDown = maxCoolDown;
                    OnCoolDown = () =>
                    {
                        IsSteerLeftBtnDown = true;
                    };
                }
            }
            SeePlayer = true;
            IsFireBtnDown = true;
        }
        else
        {
            // we don't see him
            if (SeePlayer)
            {
                
            }
            else
            {
                IsSteerLeftBtnDown = true;
                SeePlayer = false;
            }


        }
    }

}
