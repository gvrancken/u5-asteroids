﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public GameObject ShipPrefab;

    void Start() {
        GameObject playerShip = (GameObject)Instantiate(ShipPrefab, Vector3.zero, Quaternion.identity);
        playerShip.AddComponent<PlayerInputController>();

        GameObject enemyShip = (GameObject)Instantiate(ShipPrefab, new Vector3(40, 0, 40), Quaternion.identity);
        enemyShip.AddComponent<EnemyInputController>();
    }

}
