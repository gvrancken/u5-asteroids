﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour {

    public bool IsFireBtnDown { get; protected set; }
    public bool IsSteerLeftBtnDown { get; protected set; }
    public bool IsSteerRightBtnDown { get; protected set; }
    public bool IsForwardBtnDown { get; protected set; }

}
