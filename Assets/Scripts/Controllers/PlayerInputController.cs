﻿using UnityEngine;
using System.Collections;

public class PlayerInputController : InputController {

    public KeyCode Fire = KeyCode.Space;
    public KeyCode SteerLeft = KeyCode.LeftArrow;
    public KeyCode SteerRight = KeyCode.RightArrow;
    public KeyCode GoForward = KeyCode.UpArrow;

	void Update () {
        IsFireBtnDown = Input.GetKey(Fire);
        IsSteerLeftBtnDown = Input.GetKey(SteerLeft);
        IsSteerRightBtnDown = Input.GetKey(SteerRight);
        IsForwardBtnDown = Input.GetKey(GoForward);
	}
}
