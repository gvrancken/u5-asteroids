﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

    public GameObject Bullet;

    public float RotationSpeed = 1f;
    public float MaxSpeed = 5f;
    public float FireRate = 2f;

    float _fireCoolDown = 0;
    InputController _inputController;

	// Use this for initialization
	void Start () {
        _inputController = transform.GetComponent<InputController>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_fireCoolDown > 0)
        {
            _fireCoolDown -= Time.deltaTime;
        }
        else
        {
            _fireCoolDown = 0;
        }

        if (_inputController.IsFireBtnDown) 
        {
            if (_fireCoolDown <= 0)
            {
                // shoot!
                Instantiate(Bullet, transform.position, transform.rotation);
                _fireCoolDown += FireRate;
            }
        }
	}

    void OnCollisionEnter(Collision col) 
    {
        if (col.transform.GetComponent<Asteroid>() != null)
        {
            // ship collided with asteroid!
            Destroy(gameObject);
        }
    }

}
