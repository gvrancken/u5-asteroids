﻿using UnityEngine;
using System.Collections;

public class Target : MonoBehaviour {

    public float rotateSpeed = 100f;
    public GameObject targetObject;

    Vector3 centerPos;

	// Use this for initialization
	void Start () {
        SetNewPosition();
        transform.position = centerPos;
	}
	
    // Update is called once per frame
    void Update () {
        targetObject.transform.RotateAround(targetObject.transform.parent.position, Vector3.up, rotateSpeed * Time.deltaTime);
    }

    public void SetNewPosition() {
        centerPos = new Vector3(Random.Range(0, 30), 0, Random.Range(0, 30));
        transform.position = centerPos;
    }

    public Vector3 GetPosition() {
        return transform.position + targetObject.transform.localPosition;
    }

}
