﻿using UnityEngine;
using System.Collections;

public class Thruster : MonoBehaviour {

    public float AccelerationSpeed = 1f;

    public bool IsLeft;
    public bool IsForward;
    public bool IsRight;

    GameObject _thrustIndicator; 

    Rigidbody _rb;
    InputController _ipc;
	

    void Start () {
        _ipc = transform.parent.GetComponent<InputController>();
        _rb = transform.parent.GetComponent<Rigidbody>();
        _thrustIndicator = transform.Find("Fire").gameObject;
	}

	void Update () {
        _thrustIndicator.SetActive(false);
        if ((IsLeft && _ipc.IsSteerLeftBtnDown) ||
            (IsRight && _ipc.IsSteerRightBtnDown) ||
            (IsForward && _ipc.IsForwardBtnDown))
        {
            _rb.AddForceAtPosition(transform.forward , transform.position, ForceMode.Impulse);
            _thrustIndicator.SetActive(true);
        }
   
	}
}
